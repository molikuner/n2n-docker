variable "IMAGE_BASE_NAME" {}
variable "PACKAGE_VERSION" {}
variable "RELEASE_VERSION" {}
variable "OUTPUT_TYPE" {
  // should be either of "container" or "binary"
  default = "binary"
}
variable "BUILD_ALPINE_PRODUCTS" {
  default = equal("container", OUTPUT_TYPE)
}

target "default" {
  matrix = {
    product = BUILD_ALPINE_PRODUCTS ? [
      "supernode", "edge", "supernode-alpine", "edge-alpine"
    ] : [
      "supernode", "edge"
    ]
  }
  name = product
  target = product
  tags = notequal("", IMAGE_BASE_NAME) ? [
    notequal("", PACKAGE_VERSION) ?
      "${IMAGE_BASE_NAME}/${product}:${PACKAGE_VERSION}" : "",
    equal(RELEASE_VERSION, PACKAGE_VERSION) ?
      "${IMAGE_BASE_NAME}/${product}" : ""
  ] : []
  pull = true
  output = equal("container", OUTPUT_TYPE) ? [
    "type=registry"
  ] : notequal("", PACKAGE_VERSION) ? [
    "type=local,dest=${product}-${PACKAGE_VERSION}"
  ] : []
  platforms = [
    "linux/386",
    "linux/amd64",
    "linux/arm/v6",
    "linux/arm/v7",
    "linux/arm64/v8",
    "linux/ppc64le",
    "linux/s390x"
  ]
}
