###### BUILD ######
FROM alpine AS build

RUN apk -u add git autoconf automake build-base zstd-dev libcap-dev zstd-static libcap-static

COPY ./n2n/ /n2n/

WORKDIR /n2n

RUN ./autogen.sh
RUN ./configure --with-zstd --enable-cap LDFLAGS="-O3 -s -pie -static -Xlinker --no-dynamic-linker"
RUN make edge supernode # tools

###### DISTROLESS ######
# SUPERNODE
FROM scratch AS supernode
COPY --from=build /n2n/supernode /

ENTRYPOINT ["/supernode"]
CMD ["/supernode.conf", "-f"]

# EDGE
FROM scratch AS edge
COPY --from=build /n2n/edge /

ENTRYPOINT ["/edge"]
CMD ["/edge.conf", "-f"]

###### ALPINE ######
# SUPERNODE
FROM alpine AS supernode-alpine
COPY --from=build /n2n/supernode /

ENTRYPOINT ["/supernode"]
CMD ["/supernode.conf", "-f"]

# EDGE
FROM alpine AS edge-alpine
COPY --from=build /n2n/edge /

ENTRYPOINT ["/edge"]
CMD ["/edge.conf", "-f"]

